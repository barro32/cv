# Daniel Barrington
<daniel.barrington@gmail.com> | +353868841745 | 10 Home Villas, Dublin, D04 A7V0, Ireland

## Front End Engineer
**TypeScript | React | Nodejs | AWS | Git | Docker | Webpack**

Front End focused engineer with experience across Full Stack and DevOps.  
I love cloud build tool configs and CI/CD pipelines as much as slick designs and intuitive user interfaces.
From working on small startup teams, I've built production experience with AWS and GCloud infrastructure, DB architecture, API development, accessible UIs, wire framing and even logo design.
I take pride in delivering engaging, easy-to-use applications, whether for scalable consumer apps or complex enterprise services.

## Experience
**Senior Front End Engineer | HealthUnlocked**  
2019 - now | London, UK

HealthUnlocked, a social network of health and wellness support groups.
- Utilise the latest languages, frameworks, and libraries including TypeScript, React, NextJS, Redux, and Svelte
- Develop a platform with over a million users
- Modernise legacy codebase to latest standards
- Maintain and enhance Clojure API and Python CLI developer tools

**Front End Lead | Pancentric Digital**  
2017 - 2019 | London, UK

Pancentric, a digital agency servicing multinational corporations and developing insurance products.
- Master the right tool for the job including Java, Python | Django, .NET | Sitecore & Umbraco
- Focus on modern frontend tech such as JavaScript ES6+ and Sass
- Update legacy codebases and architect new projects to deliver on tight customer timelines
- Define build processes and CI/CD jobs to accelerate workflow for my whole team

**Founder | Ace**  
2016 - 2017 | Dublin, Ireland

Ace, an app to connect sporting event organisers and participants. Organisers can create events, fill out relevant details and collect fees through the app. Participants can search and filter upcoming events, view details and other participants, and register themselves for the event.
- Innovate a new consumer concept and built app from zero to launch
- Design, develop and test a mobile-first web app from front to backend using Angularjs, Cordova, Ionic, Nodejs, MySQL

**Full Stack Engineer | Lightbox**  
2014 | Dublin, Ireland

Lightbox, an award winning digital agency providing clients with engaging web, mobile and social media solutions.
- Quickly contribute to new and existing projects with rapid deliverables
- Defined technical specs, timelines and budgets for new project proposals

**Front End Engineer | Tomnod**  
2011 - 2013 | San Diego, California

Tomnod, a crowdsourcing platform to analyse satellite imagery. Users are tasked with identifying specific features; we extract the crowd consensus to confidently analyse large areas of the globe.
- Develop intuitive UI with Backbone, Underscore and Google Maps/OpenLayers to allow users to easily identify points of interest
- Create gamification and community feedback to keep contributors accurate and engaged
- As employee #1; help grow Tomnod to 1M users and acquisition by DigitalGlobe

**Web Dev | Freelance**  
2007 - 2011 | Dublin, Ireland


**BSc Computer Science | UCD**  
2003 - 2007 | UCD, Dublin, Ireland


## Links
In addition to my professional work, I enjoy prototyping in new frontend frameworks (React, Vue, Angular) and developing game concepts in Unity3D.
- [Gitlab](https://gitlab.com/barro32)
- [Stackoverflow](https://stackoverflow.com/users/766966/barro32)
